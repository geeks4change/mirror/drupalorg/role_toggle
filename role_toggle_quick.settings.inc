<?php
/**
 * @file role_toggle_quick.settings.inc
 */

function role_toggle_quick_settings($form, &$form_state) {
  list($key, $presets) = _role_toggle_quick_get_settings();

  $form['role_toggle_quick_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Query key'),
    '#default_value' => $key,
  );
  $form['role_toggle_quick_presets'] = array(
    '#type' => 'role_toggle_quick_presets',
    '#roles' => role_toggle_user_roles(),
    '#presets' => $presets,
  );
  $form = system_settings_form($form);
  return $form;
}

function role_toggle_quick_presets_element_validate(&$element, &$form_state) {
  $element_value =& drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  $keys = $element_value['keys'];
  $values = $element_value['values'];
  $keyed_values = array();
  foreach ($keys as $index => $key) {
    if ($key !== '') {
      $keyed_values[$key] = $values[$index];
    }
  }
  $element_value = $keyed_values;
}

/**
 * Process function.
 */
function role_toggle_quick_presets_process($element) {
  // Add an empty line.
  $element['#presets'][''] = array();
  $preset_keys = array_keys($element['#presets']);
  $presets = array_values($element['#presets']);
  $roles = $element['#roles'];


  foreach ($preset_keys as $index => $key) {
    $preset = $presets[$index];
    $element['keys'][$index] = array(
      '#type' => 'textfield',
      '#default_value' => (string)$key,
    );

    foreach ($roles as $rid => $_) {
      $checked = !empty($preset[$rid]);
      $element['values'][$index][$rid] = array(
        '#type' => 'checkbox',
        '#default_value' => $checked,
      );
    }
  }
  return $element;
}

/**
 * Theme function.
 */
function theme_role_toggle_quick_presets($variables) {
  $element = $variables['element'];
  $roles = $element['#roles'];
  $presets = array_values($element['#presets']);
  $header = array(t('Preset key')) + $roles;

  $rows = array();
  foreach ($presets as $preset_key => $preset) {
    $row = array();
    $row[] = drupal_render($element['keys'][$preset_key]);
    foreach ($roles as $rid => $_) {
      $role_form_element = $element['values'][$preset_key][$rid];
      $row[] = drupal_render($role_form_element);
    }
    $rows[] = $row;
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}
